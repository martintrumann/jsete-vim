runtime! syntax/html.vim
unlet b:current_syntax

syn region  jseteComment  contained start=+/\*+  end=+\*/+ contains=jsCommentTodo,@Spell fold extend keepend

syn keyword jseteKeywords contained if
syn keyword jseteKeywords contained else
syn keyword jseteKeywords contained switch
syn keyword jseteKeywords contained while
syn keyword jseteKeywords contained for

syn cluster jseteJavaScript contains=jsString,jsNumber,jsGlobalObjects,jsStorageClass,jseteKeywords,jsFuncCall,jsBooleanTrue,jsBooleanFalse,jseteComment

syn region  jseteScriptlet	matchgroup=jseteTag start=/<%[_-]\?/         keepend end=/[_-]\?%>/ contains=@jseteJavaScript
syn region  jseteExpr		matchgroup=jseteTag start=/<%[_-]\?.*[=~]/    keepend end=/[_-]\?%>/ contains=@jseteJavaScript

syn cluster jsete contains=jseteExpr,jseteScriptlet

syn clear htmlTag
syn region htmlTag start=+<[^/%]+ end=+>+ contains=htmlTagN,htmlString,htmlArg,htmlValue,htmlTagError,htmlEvent,htmlCssDefinition,@htmlPreproc,@htmlArgCluster,@jsete

syn include @htmlCss syntax/css.vim
syn region cssStyle start=+<style+ keepend end=+</style>+ contains=@htmlCss,htmlTag,htmlEndTag,htmlCssStyleComment,@htmlPreproc,@jsete
syn match  htmlCssStyleComment contained "\(<!--\|-->\)"
syn region htmlCssDefinition matchgroup=htmlArg start='style="' keepend matchgroup=htmlString end='"' contains=css.*Attr,css.*Prop,cssComment,cssLength,cssColor,cssURL,cssImportant,cssError,cssString,@htmlPreproc

syn region cssDefinition transparent matchgroup=cssBraces start='{' end='}' contains=cssTagName,cssAttributeSelector,cssClassName,cssIdentifier,cssAtRule,cssAttrRegion,css.*Prop,cssComment,cssValue.*,cssColor,cssURL,cssImportant,cssCustomProp,cssError,cssStringQ,cssStringQQ,cssFunction,cssUnicodeEscape,cssVendor,cssDefinition,cssHacks,cssNoise,@jsete fold

syn region htmlComment                start=+<!+      end=+>+   contains=htmlCommentPart,htmlCommentError,@Spell

hi def link htmlStyleArg htmlString

hi def link jseteKeywords Conditional
hi def link jsetedeclarations Conditional

hi def link jseteTag htmlTag
hi def link jseteComment jsComment

let b:current_syntax = "jsete"
