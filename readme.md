# Javascript embedding template engine syntax for vim

This adds better syntax for template engines that use embedded Javascript,
like ejs and eta.

Requires [vim-javascript](https://github.com/pangloss/vim-javascript)

![](img/demo.png)

## License

Copyright (c) Martin Trumann. Distributed under the same terms as Vim itself.
See :help license.

